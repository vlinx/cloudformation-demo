# cloudformation-demo

Provide a Cloud formation template defining a redundant, secure and cost effective LAMP stack within AWS.

## Installation

Our architecture consists of the following resources:

- VPC with 4 Subnets
- Minimum of 2 EC2 Instances scaled automatically with EC2 AutoScaling
- Application Elastic Load Balancer
- Multi-AZ RDS MySQL Cluster

All resources must be launched or created within the VPC. Automate the creation of AWS Resources required to deploy our application by creating a Cloudformation Template.

To be sure that our system is healthy and working fine, thus, some monitoring tools (mainly CloudWatch and Route 53) combined with SNS to notify whenever something critical occurs.

If a stack does not exist, it will be created. If it does exist and the template file has changed, the stack will be updated. If the parameters are different, the stack will also be updated.

CloudFormation stacks can take a while to provision, if you are curious about its status, use the AWS web console or one of the CloudFormation CLI's.

## Usage

TODO: Write usage instructions

## Contributing

1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D

## History

TODO: Write history

## Credits

TODO: Write credits

## License

TODO: Write license